package agency.cinnamon.reactivewifi;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.support.annotation.RequiresPermission;
import android.util.Log;

import com.jakewharton.rxrelay2.PublishRelay;
import com.jakewharton.rxrelay2.Relay;

import java.util.List;

import io.reactivex.Observable;

import static android.Manifest.permission.ACCESS_WIFI_STATE;
import static android.Manifest.permission.CHANGE_WIFI_STATE;

public final class ReactiveWifi {

    private static final String TAG = ReactiveWifi.class.getSimpleName();

    private ReactiveWifi() {

    }

    @RequiresPermission(allOf = {
            ACCESS_WIFI_STATE, CHANGE_WIFI_STATE
    })
    public static Observable<List<ScanResult>> observeWifiAccessPoints(final Context context) {
        final Relay<List<ScanResult>> relay = PublishRelay.create();

        final WifiManager wifiManager = (WifiManager) context.getApplicationContext()
                .getSystemService(Context.WIFI_SERVICE);
        if (wifiManager == null) {
            Log.d(TAG, "Cannot access WifiManager");
            return Observable.empty();
        }

        wifiManager.startScan();

        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);

        final BroadcastReceiver wifiScanReceiver = new BroadcastReceiver() {
            @SuppressLint("MissingPermission")
            @Override
            public void onReceive(Context context, Intent intent) {
                wifiManager.startScan();
                relay.accept(wifiManager.getScanResults());
            }
        };

        context.registerReceiver(wifiScanReceiver, intentFilter);

        return relay;
    }
}
